rm website.zip
ssh website-gitlab "rm -rf /var/www/thesis-website/*"
zip -r website.zip * -x '*.git*/*' -x '*node_modules*/*' -x '*build/*'
scp website.zip website-gitlab:/var/www/thesis-website/
ssh website-gitlab "unzip /var/www/thesis-website/website.zip -d /var/www/thesis-website/"
ssh website-gitlab "cd /var/www/thesis-website/ && docker build --no-cache -t thesis-website . && docker run -p 4200:4200 -itd thesis-website"