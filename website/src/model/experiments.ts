import { writable, type Writable } from "svelte/store"

type Metrics = {
    fid: number | null,
    kid: number | null,
    ssim: number | null,
    psnr: number | null,
    phv: number[],
    accuracy: number | null,
    accuracyTop3: number | null
}

type Experiment = {
    title: string,
    folderName: string,
    ours: boolean,
    date: Date,
    trainingDetails: string[],
    description: string,
    metrics: Metrics
    metricsPerEpoch: Map<number, Metrics> | null
    enabled: boolean
}

export const experiments: Writable<Experiment[]> = writable([
    {
        title: "CycleGAN",
        folderName: "cyclegan",
        ours: false,
        date: new Date("2017-01-1"),
        trainingDetails: [],
        description: "",
        metrics: {
            fid: 51.79,
            kid: null,
            ssim: 0.19987812071667335,
            psnr: 13.268511706333323,
            phv: [0.4714, 0.4582, 0.3441, 0.8616],
            accuracy: 0.215,
            accuracyTop3: 0.333,
        },
        metricsPerEpoch: null,
        enabled: true
    },
    {
        title: "Supervised PatchNCE",
        folderName: "asp_zero_uniform",
        date: new Date("2023-03-10"),
        trainingDetails: [
            "Model name: mist_er_zero_uniform",
            "Weighting function: uniform",
            "Scheduling function: zero"
        ],
        description: `This model uses basically the PatchNCE loss from CUT paper (Contrastive Learning for Unpaired Image-to-Image Translation) in a supervised fashion.
        The authors proposed a patch based loss which aims to preserve structural information by learning similar embedding for the patches for both the input and the generated image.
        `,
        metrics: {
            fid: 46.98,
            kid: null,
            ssim: 0.21566622420836257,
            psnr: 14.597421758820333,
            phv: [0.4444, 0.4335, 0.3212, 0.8434],
            accuracy: 0.22,
            accuracyTop3: 0.329,
        },
        metricsPerEpoch: null,
        ours: false,
        enabled: true
    },
    {
        title: "Adaptive Supervised PatchNCE",
        folderName: "asp_lambda_linear",
        date: new Date("2023-03-10"),
        trainingDetails: [
            "Model name: mist_er_lambda_linear",
            "Weighting function: linear",
            "Scheduling function: lambda"
        ],
        description: `This model uses the same loss like the Supervised PatchNCE plus an additional weighing which is applied to the loss, hence the name 'Adaptive'.
        Since the image pairs contain severly inconsistent parts, the idea is to not punish the network for not predicting severly inconsistent locations.`,
        metrics: {
            fid: 42.03,
            kid: null,
            ssim: 0.19908530127306606,
            psnr: 14.123347080004885,
            phv: [0.4419, 0.4298, 0.3174, 0.8465],       
            accuracy: 0.238,
            accuracyTop3: 0.364,
        },
        metricsPerEpoch: null,
        ours: false,
        enabled: true
    },
    {
        title: "Thesis Finetuned InstructPix2Pix DDPM 200 steps",
        folderName: "stable_diffusion_fine_tune_ddpm",
        date: new Date("2024-07-10"),
        trainingDetails: [
        ],
        description: `A pretrained stable diffusion model was fine-tuned with the InstructPix2Pix script from diffusers (https://huggingface.co/docs/diffusers/en/training/instructpix2pix).
        `,
        metrics: {
            "ssim": 0.2220684397805342,
            "psnr": 14.5045542150275,
            "fid": 30.860544383547563,
            "kid": 0.0025458864294445062,
            "phv": [
                0.40546484375,
                0.358015625,
                0.2789638671875,
                0.82751220703125
            ]
        },
        metricsPerEpoch: null,
        ours: true,
        enabled: true
    },
    // {
    //     title: "Plug and Play: Inference with injection",
    //     folderName: "stable_diffusion_pnp",
    //     date: new Date("2024-10-28"),
    //     trainingDetails: [
    //     ],
    //     description: `Inference with Plug and Play
    //     `,
    //     metrics: {
    //         fid: 104.87,
    //         kid: null,
    //         ssim: 0.22195539269603126,
    //         psnr: 13.689894992765913,
    //         phv: [0.5404, 0.5015, 0.3697, 0.8709],
    //         accuracy: null,
    //         accuracyTop3: null
    //     },
    //     metricsPerEpoch: null,
    //     ours: true,
    //     enabled: true
    // },
    // {
    //     title: "Inversion 1000 steps - original model",
    //     folderName: "inversion_original_sd",
    //     date: new Date("2024-10-28"),
    //     trainingDetails: [
    //     ],
    //     description: `Inference with Plug and Play
    //     `,
    //     metrics: {
    //         fid: 89.83,
    //         kid: null,
    //         ssim: 0.19508487526145474,
    //         psnr: 12.276004445995351,
    //         phv: [0.5473, 0.5377, 0.3835, 0.8809],
    //         accuracy: null,
    //         accuracyTop3: null
    //     },
    //     metricsPerEpoch: null,
    //     ours: true,
    //     enabled: true
    // },
    // {
    //     title: "Inversion 1000 steps - H&E finetuned model",
    //     folderName: "inversion_he_finetune",
    //     date: new Date("2024-11-14"),
    //     trainingDetails: [],
    //     description: `Inference with inversion from finetuned stable diffusion trained on H&E
    //     `,
    //     metrics: {
    //         fid: 74.50,
    //         kid: null,
    //         ssim: 0.195290274079297,
    //         psnr: 12.556204255578022,
    //         phv: [0.5174, 0.5047, 0.3635, 0.8727],
    //         accuracy: null,
    //         accuracyTop3: null
    //     },
    //     metricsPerEpoch: null,
    //     ours: true,
    //     enabled: true
    // },
    // {
    //     title: "DDS - Delate Denoising Score",
    //     folderName: "dds",
    //     date: new Date("2024-11-15"),
    //     trainingDetails: [
    //     ],
    //     description: `Delta denoising score (DDS) with reference identitiy branch from finetuned H&E SD. And translation branch from finetuned H&E->ER SD InstructPix2Pix
    //     `,
    //     metrics: {
    //         fid: 85.34,
    //         kid: null,
    //         ssim: 0.21262447994478914,
    //         psnr: 13.58305609773195,
    //         phv: [0.4966, 0.4783, 0.3526, 0.8652],
    //         accuracy: null,
    //         accuracyTop3: null
    //     },
    //     metricsPerEpoch: null,
    //     ours: true,
    //     enabled: true
    // },
])
