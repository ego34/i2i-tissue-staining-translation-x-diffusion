export type Todo = {
    description: string
    done: boolean
}

export const todos: Todo[] = [
    {
        description: "Implement CUT loss into InstructPix2Pix.",
        done: true
    },
    {
        description: "Find paper for CUT loss for diffusion inference.",
        done: true
    },
    {
        description: "Find paper for CUT loss for finetuning stable diffusion.",
        done: true
    },
    {
        description: "Research PatchNCE for inference.",
        done: true
    },
    {
        description: "Try different prompts for finetuning with InstructPix2Pix (Google Slides). More detailed stain specific ER/KI67 Prompt, no vague promopt.",
        done: false
    },
    {
        description: "Try different fine tuning techniqes fith InstructPix2Pix like LoRA.",
        done: false
    },
    {
        description: "Check ASP code base for shared network F, is it the encoder network? Is it an auxilliary network?",
        done: false
    },
    {
        description: "Check ASP code base for shared network F, is it the encoder network? Is it an auxilliary network?",
        done: false
    },
    {
        description: "Research method to add HE conditioning input to other encoder layers, also decoder maybe.",
        done: false
    },
    {
        description: "Try inference with >20 steps with instrcut pix2pix.",
        done: false
    },
    {
        description: "Add experiment Supervised PatchNCE",
        done: true
    },
    {
        description: "Add experiment CycleGAN",
        done: true
    },
    {
        description: "Add experiment Pix2Pix",
        done: false
    }
]