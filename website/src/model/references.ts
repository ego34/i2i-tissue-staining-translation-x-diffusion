type Reference = {
    title: string,
    date: Date,
    citations: number,
    summary: string,
    tags: string[],
    url: string,
    type: string
}

export const referenceSections: Record<string, Reference[]> = {
    "Diffusion Models": [
        {
            type: "Paper",
            title: "Deep Unsupervised Learning using Nonequilibrium Thermodynamics",
            date: new Date("2015-03-12"),
            citations: 5042,
            summary: "The first paper proposing diffusion process to generate Data.",
            tags: ["Diffusion"],
            url: "https://arxiv.org/abs/1503.03585"
        },
        {
            type: "Paper",
            title: "Denoising Diffusion Implicit Models",
            date: new Date("2020-10-06"),
            citations: 4350,
            summary: `
            The original DDPM method for generating images is slow, since the forward diffusion process usually takes T~1000 to make the distribution of xT appear close to Gaussian. However, this means the backward diffusion process also takes 1000 steps.
            To accelerate sampling, we present denoising diffusion implicit models (DDIMs), a more efficient class of iterative implicit probabilistic models with the same training procedure as DDPMs
            DDIM is a method to take any model trained on DDPM loss, and use it to sample with some steps skipped, sacrificing an adjustable amount of quality.
            `,
            tags: ["DDIM", "Diffusion"],
            url: "https://arxiv.org/abs/2010.02502"
        },
        {
            type: "Paper",
            title: "Denoising Diffusion Probabilistic Models",
            date: new Date("2020-07-19"),
            citations: 11732,
            summary: `
            High quality image synthesis results using diffusion probabilistic models.
            DDPM improves upon the previous method by variational inference.
            DDPM models can be trained by maximizing the likelihood of the data by minimizing the variational bound on negative log likelihood. 
            `,
            tags: ["DDPM", "Diffusion"],
            url: "https://arxiv.org/abs/2006.11239"
        },
    ],
    "Diffusion I2I x content preservation": [
        {
            title: "Diffusion-based image translation using disentangled style and content representation",
            date: new Date("2022-09-22"),
            citations: 112,
            summary: `
            The authors propose a diffusion-based unsupervised image translation method, DiffuseIT, using disentangled style and content representation.
            This method can be applied to pre-trained diffusion models.
            The methods extracts intermediate keys of multihead self attention layer from ViT model and used them as the content preservation loss.
            The content preservation loss consists of a CUT loss and a SSIM based loss.
            `,
            tags: ["Diffusion", "CUT", "Finetuning", "DiffuseIT"],
            url: "https://github.com/cyclomon/DiffuseIT",
            type: "Paper"
        },
        {
            title: "Zero-Shot Contrastive Loss for Text-Guided Diffusion Image Style Transfer",
            date: new Date("2023-03-15"),
            citations: 40,
            summary: `
            The authors propose a zeroshot contrastive loss (ZeCon) for diffusion models that doesn’t require additional fine-tuning or auxiliary networks.
             Our
approach is based on the observation that a pre-trained diffusion model already contains spatial information in its embedding that can be used to maintain content through patchwise contrastive loss between the input image and generated
images.
            By leveraging patch-wise contrastive loss between generated samples and original image embeddings in the pre-trained diffusion model, our method can generate images with the same
semantic content as the source image in a zero-shot manner.

            `,
            tags: ["Diffusion", "Zero-Shot", "Inference", "ZeCon"],
            url: "https://github.com/YSerin/ZeCon",
            type: "Paper"
        },
        {
            type: "Paper",
            title: "Delta Denoising Score (DDS)",
            date: new Date("2023-04-14"),
            citations: 72,
            summary: `
            Delta Denoising Score is a scoring function for text-based image editing that guides
            minimal modifications of an input image towards the content described in a target prompt.
            DDS utilizes the Score Distillation Sampling (SDS) mechanism for the purpose of image editing.
            `,
            tags: ["DDS", "Diffusion", "Zero-Shot", "Inference"],
            url: "https://delta-denoising-score.github.io/"
        },
        {
            type: "Paper",
            title: "Contrastive Denoising Score (CDS) for Text-guided Latent Diffusion Image Editing",
            date: new Date("2024-04-01"),
            citations: 3,
            summary: `
            Contrastive Denoising Score (CDS) is a modification of Delta Denoising Score (DDS) using CUT loss within the DDS framework.
            Rather than employing auxiliary networks as in the original CUT approach, we leverage the intermediate features of LDM which possesses rich spatial information. 
            `,
            tags: ["CDS", "Diffusion", "Zero-Shot", "Inference"],
            url: "https://hyelinnam.github.io/CDS/"
        },
        {
            title: "DreamFusion: Text-to-3D using 2D Diffusion",
            date: new Date("2022-09-29"),
            citations: 1522,
            summary: `The authors use a pretrained 2D text-to-image diffusion model to perform text-to-3D synthesis.
            They introduce a loss based on probability density distillation that enables the use of a 2D diffusion model as a prior for optimization of a parametric image generator.
            
            `,
            tags: ["SDS", "DreamFusion"],
            url: "https://dreamfusion3d.github.io/",
            type: "Paper"
        }
    ],
    "Stain Translation": [
        {
            title: "A comparative evaluation of image-to-image translation methods for stain transfer in histopathology",
            date: new Date("2023-03-29"),
            citations: 8,
            summary: "Comparison of different methods for stain translation from H&E to MT. Most methods are GAN based. CycleGAN performs the strongest in FID and WD. Pix2Pix performs the strongest in SSIM.",
            tags: ["GAN"],
            url: "https://arxiv.org/abs/2303.17009",
            type: "Paper"
        }
    ],
    "Metrics": [
        {
            title: "GANs Trained by a Two Time-Scale Update Rule Converge to a Local Nash Equilibrium",
            date: new Date("2017-07-26"),
            citations: 12984,
            summary: "The authors introduce the 'Fréchet Inception Distance' (FID) which captures the similarity of generated images to real ones better than the Inception Score",
            tags: ["GAN", "Metric", "FID"],
            url: "https://arxiv.org/abs/1706.08500",
            type: "Paper"
        },
        {
            title: "An Improved Evaluation Framework for Generative Adversarial Networks",
            date: new Date("2018-03-20"),
            citations: 53,
            summary: "The authors show why the FID score may give inconsistent results in domain-specific tasks and propose Class-Aware Frechet Distance (CAFD).",
            tags: ["GAN", "Metric", "FID", "CAFD"],
            url: "https://arxiv.org/abs/1803.07474",
            type: "Paper"
        }
    ],
}
