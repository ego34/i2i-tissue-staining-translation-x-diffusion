type Note = {
    title: string,
    date: Date,
    text: string
}

export const notes: Note[] = [
    {
        "title": "Start of this website",
        date: new Date("2024-08-28"),
        text: "In order to collect all information at a public accessible place, this website was created."
    },
    {
        title: "Responsive for mobile and added metrics section",
        date: new Date("2024-09-02"),
        text: `Today I updated the website so its compatible with mobile resolutions. Also I added the metrics section to /experiments. Since metrics are crucial to evaluate model performance I gathered all information I could find about different metrics.`
    },
    {
        title: "Added ASP experiment and experiment descriptions",
        date: new Date("2024-09-03"),
        text: `I generated 1000 images from the ER benchmark dataset from a pretrained model from the ASP repository (https://github.com/lifangda01/AdaptiveSupervisedPatchNCE). 
        They published a couple of models with different configurations. Two for the ER dataset. 
        Interestingly different configurations from the ones from their paper. 
        I choose the one with a lambda schedule for the weighting over the one with a zero schedule which does not take into account their proposed weighting scheme at all. 
        I might also generate images from the schedule without the adaptive part to see if it makes a difference. 
        Next step is to compute metrics for the dataset. They have a script which computes metrics.`
    },
    {
        title: "Add Todo's and generate images",
        date: new Date("2024-09-16"),
        text: "Today I added the Todo's section to the website to keep track of pending and done tasks. Also I generated Images for Supervised PatchNCE Loss Model"
    }
]