export const numToChar = (num: number): string => String.fromCharCode('a'.charCodeAt(0) + num)

export const dateToString = (date: Date): string => date.toLocaleDateString(
    "en-US",
    {
        year: "numeric",
        month: "long",
        day: "numeric",
    },
)