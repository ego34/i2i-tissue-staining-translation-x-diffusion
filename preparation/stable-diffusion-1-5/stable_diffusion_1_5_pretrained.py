import matplotlib.pyplot as plt
import torch
from diffusers import DiffusionPipeline, DPMSolverMultistepScheduler, DDIMScheduler, AutoencoderKL

model_id = "runwayml/stable-diffusion-v1-5"
pipeline = DiffusionPipeline.from_pretrained(model_id, torch_dtype=torch.float16, use_safetensors=True)
pipeline.to("cuda")
generator = torch.Generator("cuda").manual_seed(1)

# vae = AutoencoderKL.from_pretrained("stabilityai/sd-vae-ft-mse", torch_dtype=torch.float16).to("mps")
# pipeline.vae = vae

prompt = "A frog with a horsebody wearing a cowboy hat"

image = pipeline(prompt, generator=generator, num_inference_steps=10).images[0]

image.save("image_6_SickVAE.png")
