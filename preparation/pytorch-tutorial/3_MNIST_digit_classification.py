import torch
import torchvision
from torchvision import datasets
from torchvision.transforms import ToTensor
from torch.utils.data.dataloader import default_collate
from torch import nn
from torch.utils.data import DataLoader
import matplotlib.pyplot as plt
import numpy as np
import torch.nn.functional as F
import torchvision.transforms as transforms

device = (
    "cuda"
    if torch.cuda.is_available()
    else "mps"
    if torch.backends.mps.is_available()
    else "cpu"
)
print(f"Using {device} device")

# hyper parameters
learning_rate = 0.001
batch_size = 64
epochs = 20

# Scaled Mean: 0.13062754273414612 
# Scaled Std: 0.30810779333114624
transform = transforms.Compose([
        transforms.ToTensor()
        ,transforms.Normalize((0.1307,), (0.3081,)) # (data - mean) / std
        ])

training_data = datasets.MNIST(
    root="data/MNIST",
    train=True,
    download=True,
    transform=transform
)

test_data = datasets.MNIST(
    root="data/MNIST",
    train=False,
    download=True,
    transform=transform
)

train_dataloader = DataLoader(training_data, batch_size=64)
test_dataloader = DataLoader(test_data, batch_size=64)

class NeuralNetwork(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(1, 2, 3, padding=1)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(2, 16, 3, padding=1)
        self.convX = nn.Conv2d(1, 16, 3, padding=1)
        self.fc1 = nn.Linear(16*28*28, 130)
        self.fc2 = nn.Linear(130, 60)
        self.fc3 = nn.Linear(60, 10)

    def forward(self, x):
        # -> n, 1, 28, 28
        x = F.relu(self.conv1(x))  # -> n, 2, 28, 28
        x = F.relu(self.conv2(x))  # -> n, 16, 28, 28
        x = x.view(-1, 16*28*28)                 # -> n, 392
        x = F.relu(self.fc1(x))               # -> n, 130
        x = F.relu(self.fc2(x))               # -> n, 60
        x = self.fc3(x)                       # -> n, 10
        return x

model = NeuralNetwork().to(device)
print(model)

def train_loop(dataloader, model, loss_fn, optimizer):
    size = len(dataloader.dataset)
    model.train()
    
    # mini-batch gradient descent
    for batch, (X, y) in enumerate(dataloader):
        pred = model(X.to(device))
        loss = loss_fn(pred, y.to(device))
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()

        if batch % 100 == 0:
            loss, current = loss.item(), batch * batch_size + len(X)
            print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")

def test_loop(dataloader, model, loss_fn):
    model.eval()
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    test_loss, correct = 0, 0

    with torch.no_grad():
        for X, y in dataloader:
            pred = model(X.to(device))
            test_loss += loss_fn(pred, y.to(device)).item()
            correct += (pred.argmax(1) == y.to(device)).type(torch.float).sum().item()

    test_loss /= num_batches
    correct /= size
    print(f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss:>8f} \n")

loss_fn = nn.CrossEntropyLoss()
optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)

for t in range(epochs):
    print(f"Epoch {t+1}\n-------------------------------")
    train_loop(train_dataloader, model, loss_fn, optimizer)
    test_loop(test_dataloader, model, loss_fn)
print("Done!")

def imshow(img):
    img = img / 2 + 0.5  # unnormalize
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()


# get some random training images
batch, (X, y) = next(enumerate(train_dataloader))

# show images
# imshow(torchvision.utils.make_grid(X))
# print(X.shape)

# conv1 = nn.Conv2d(1, 2, 3, 1, 1)

# x = conv1(X)
# print(x.shape)
# pool = nn.MaxPool2d(2, 2)

# x = pool(x)
# print(x.shape)
# conv2 = nn.Conv2d(2, 8, 3)

# x = conv2(x)
# print(x.shape)

# x = pool(x)
# print(x.shape)
