import torch
import torchvision
from torchvision import datasets
from torchvision.transforms import ToTensor
from torch.utils.data.dataloader import default_collate
from torch import nn
from torch.utils.data import DataLoader
import matplotlib.pyplot as plt
import numpy as np
import torch.nn.functional as F
import torchvision.transforms as transforms

device = (
    "cuda"
    if torch.cuda.is_available()
    else "mps"
    if torch.backends.mps.is_available()
    else "cpu"
)
print(f"Using {device} device")

# hyper parameters
learning_rate = 0.0005
batch_size = 256
epochs = 50

# Scaled Mean: 0.13062754273414612 
# Scaled Std: 0.30810779333114624
transform = transforms.Compose([
        transforms.ToTensor()
        ,transforms.Normalize((0.1307,), (0.3081,)) # (data - mean) / std
        ])

training_data = datasets.MNIST(
    root="data/MNIST",
    train=True,
    download=True,
    transform=transform
)

test_data = datasets.MNIST(
    root="data/MNIST",
    train=False,
    download=True,
    transform=transform
)

train_dataloader = DataLoader(training_data, batch_size=64)
test_dataloader = DataLoader(test_data, batch_size=64)

# checking the dataset
print("Train data")
for batch, (X, y) in enumerate(train_dataloader):
    print("Image batch dimensions: ", X.size())
    print("Image batch shape: ", X.shape)
    print("labels: ", y[:10])
    break

print("Test data")
for batch, (X, y) in enumerate(test_dataloader):
    print("Image batch dimensions: ", X.size())
    print("Image batch shape: ", X.shape)
    print("labels: ", y[:10])
    break


