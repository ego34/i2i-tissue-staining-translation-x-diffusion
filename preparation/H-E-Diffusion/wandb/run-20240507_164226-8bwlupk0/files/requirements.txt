Automat==20.2.0
Babel==2.8.0
Brlapi==0.8.3
Brotli==1.0.9
Cython==0.29.28
GitPython==3.1.43
Jinja2==3.0.3
Mako==1.1.3
Markdown==3.4.1
MarkupSafe==2.0.1
MarkupSafe==2.1.2
Pillow==9.0.1
PyGObject==3.42.1
PyHamcrest==2.0.2
PyJWT==2.3.0
PyNaCl==1.5.0
PyQRCode==1.2.1
PyQt5-sip==12.9.1
PyQt5==5.15.6
PyTrie==0.4.0
PyYAML==5.4.1
Pygments==2.11.2
SCons==4.0.1
SecretStorage==3.3.1
Twisted==22.1.0
Werkzeug==2.2.3
absl-py==1.4.0
accelerate==0.29.3
aiohttp==3.9.5
aiosignal==1.3.1
appdirs==1.4.4
apt-xapian-index==0.49
apturl==0.5.2
astunparse==1.6.3
async-timeout==4.0.3
attrs==21.2.0
autobahn==21.11.1
awscli==1.22.34
backcall==0.2.0
base58==1.0.3
bcrypt==3.2.0
beautifulsoup4==4.10.0
beniget==0.4.1
blinker==1.4
botocore==1.23.34
cachetools==5.3.0
cbor==1.0.0
certifi==2020.6.20
cffi==1.15.0
chardet==4.0.0
click==8.0.3
cloud-init==24.1.3
cmake==3.26.0
colorama==0.4.4
command-not-found==0.3
configobj==5.0.6
constantly==15.1.0
cryptography==3.4.8
cupshelpers==1.0
cycler==0.11.0
datasets==2.19.0
dbus-python==1.2.18
decorator==4.4.2
defer==1.0.6
devscripts==2.22.1ubuntu1
diffusers==0.27.2
dill==0.3.8
distlib==0.3.4
distro-info==1.1+ubuntu0.2
distro==1.7.0
dnspython==2.1.0
docker-pycreds==0.4.0
docutils==0.17.1
duplicity==0.8.21
ecdsa==0.18.0b1
entrypoints==0.4
fasteners==0.14.1
filelock==3.10.0
filelock==3.6.0
flatbuffers==1.12.1-git20200711.33e2d80-dfsg1-0.6
flatbuffers==23.5.26
fonttools==4.29.1
frozenlist==1.4.1
fs==2.4.12
fsspec==2023.9.2
fuse-python==1.0.2
future==0.18.2
gast==0.5.2
gitdb==4.0.11
google-auth-oauthlib==0.4.6
google-auth==2.16.1
google-pasta==0.2.0
gpg==1.16.0
grpcio==1.51.3
gssapi==1.6.12
h5py==3.9.0
html5lib==1.1
httplib2==0.20.2
huggingface-hub==0.22.2
hyperlink==21.0.0
idna==3.3
importlib-metadata==4.6.4
incremental==21.3.0
iotop==0.6
ipaclient==4.9.8
ipalib==4.9.8
ipaplatform==4.9.8
ipapython==4.9.8
ipykernel==6.7.0
ipython==7.31.1
ipython_genutils==0.2.0
jax==0.4.13
jedi==0.18.0
jeepney==0.7.1
jmespath==0.10.0
jsonpatch==1.32
jsonpointer==2.0
jsonschema==3.2.0
jupyter-client==7.1.2
jupyter-core==4.9.1
keras==2.12.0
keyring==23.5.0
kiwisolver==1.3.2
language-selector==0.1
launchpadlib==1.10.16
lazr.restfulclient==0.14.4
lazr.uri==1.0.6
libclang==16.0.0
lit==15.0.7
lmdb==1.0.0
lockfile==0.12.2
louis==3.20.0
lxml==4.8.0
lz4==3.1.3+dfsg
macaroonbakery==1.3.1
matplotlib-inline==0.1.3
matplotlib==3.5.1
meld==3.20.4
meson==0.61.2
ml-dtypes==0.2.0
mnemonic==0.19
monotonic==1.6
more-itertools==8.10.0
mpi4py==3.1.3
mpmath==0.0.0
msgpack==1.0.3
multidict==6.0.5
multiprocess==0.70.16
nbclient==0.5.6
nbformat==5.1.3
nest-asyncio==1.5.4
netaddr==0.8.0
netifaces==0.11.0
networkx==3.0
numpy==1.21.5
numpy==1.23.5
nvidia-cublas-cu11==11.11.3.6
nvidia-cublas-cu12==12.1.3.1
nvidia-cuda-cupti-cu11==11.8.87
nvidia-cuda-cupti-cu12==12.1.105
nvidia-cuda-nvrtc-cu11==11.8.89
nvidia-cuda-nvrtc-cu12==12.1.105
nvidia-cuda-runtime-cu11==11.8.89
nvidia-cuda-runtime-cu12==12.1.105
nvidia-cudnn-cu11==8.7.0.84
nvidia-cudnn-cu12==8.9.2.26
nvidia-cufft-cu11==10.9.0.58
nvidia-cufft-cu12==11.0.2.54
nvidia-curand-cu11==10.3.0.86
nvidia-curand-cu12==10.3.2.106
nvidia-cusolver-cu11==11.4.1.48
nvidia-cusolver-cu12==11.4.5.107
nvidia-cusparse-cu11==11.7.5.86
nvidia-cusparse-cu12==12.1.0.106
nvidia-nccl-cu11==2.19.3
nvidia-nccl-cu11==2.20.5
nvidia-nccl-cu12==2.20.5
nvidia-nvjitlink-cu12==12.1.105
nvidia-nvtx-cu11==11.8.86
nvidia-nvtx-cu12==12.1.105
oauthlib==3.2.0
olefile==0.46
opt-einsum==3.3.0
packaging==21.3
pandas==2.2.2
paramiko==2.9.3
parso==0.8.1
passlib==1.7.4
pexpect==4.8.0
pickleshare==0.7.5
pillow==10.3.0
pip==22.0.2
pip==22.0.2
platformdirs==2.5.1
pluggy==0.13.0
ply==3.11
prompt-toolkit==3.0.28
protobuf==3.12.4
protobuf==3.20.3
psutil==5.9.0
ptyprocess==0.7.0
py-ubjson==0.16.1
py==1.10.0
pyOpenSSL==21.0.0
pyRFC3339==1.1
pyarrow-hotfix==0.6
pyarrow==15.0.2
pyasn1-modules==0.2.1
pyasn1==0.4.8
pycairo==1.20.1
pycparser==2.21
pycups==2.0.1
pylibacl==0.6.0
pymacaroons==0.13.0
pyparsing==2.4.7
pypng==0.0.20
pyrsistent==0.18.1
pyserial==3.5
python-apt==2.4.0+ubuntu3
python-augeas==0.5.0
python-dateutil==2.8.1
python-dateutil==2.9.0.post0
python-debian==0.1.43+ubuntu1.1
python-ldap==3.2.0
python-lsp-jsonrpc==1.0.0
python-lsp-server==1.3.3
python-magic==0.4.24
python-snappy==0.5.3
python-yubico==1.3.3
pythran==0.10.0
pytorch-msssim==1.0.0
pytz==2022.1
pyusb==1.2.1.post1
pyxattr==0.7.2
pyxdg==0.27
pyzmq==22.3.0
qrcode==7.3.1
regex==2024.4.16
reportlab==3.6.8
requests-oauthlib==1.3.1
requests-toolbelt==0.9.1
requests==2.25.1
roman==3.3
rsa==4.8
s-tui==1.1.3
s3transfer==0.5.0
safetensors==0.4.3
scipy==1.8.0
scour==0.38.2
screen-resolution-extra==0.0.0
sentry-sdk==2.1.1
service-identity==18.1.0
setproctitle==1.3.3
setuptools==59.6.0
setuptools==59.6.0
six==1.16.0
smmap==5.0.1
sortedcontainers==2.1.0
sos==4.5.6
soupsieve==2.3.1
ssh-import-id==5.11
sympy==1.9
systemd-python==234
tensorboard-data-server==0.7.0
tensorboard-plugin-wit==1.8.1
tensorboard==2.16.1
tensorflow-estimator==2.12.0
tensorflow-io-gcs-filesystem==0.32.0
tensorflow==2.12.0
termcolor==2.4.0
terminator==2.1.1
tf-keras==2.15.0
tokenizers==0.19.1
torch==1.8.0a0+unknown
torch==2.2.0+cu118
torcheval==0.0.7
torchvision==0.17.0+cu118
torchvision==0.8.0a0
tornado==6.1
tqdm==4.57.0
tqdm==4.66.2
traitlets==5.1.1
transformers==4.40.0
triton==2.2.0
triton==2.3.0
txaio==21.2.1
typing-extensions==3.10.0.2
typing_extensions==4.8.0
tzdata==2024.1
u-msgpack-python==2.3.0
ubuntu-drivers-common==0.0.0
ubuntu-pro-client==8001
ufoLib2==0.13.1
ufw==0.36.1
ujson==5.1.0
unattended-upgrades==0.1
unicodedata2==14.0.0
unidiff==0.5.5
urllib3==1.26.5
urllib3==2.2.1
urwid==2.1.2
usb-creator==0.3.7
virtualenv==20.13.0+ds
wadllib==1.3.6
wandb==0.16.6
wcwidth==0.2.5
webencodings==0.5.1
wheel==0.37.1
wrapt==1.14.1
wsaccel==0.6.3
xdg==5
xkit==0.0.0
xxhash==3.4.1
yarl==1.9.4
zipp==1.0.0
zope.interface==5.4.0