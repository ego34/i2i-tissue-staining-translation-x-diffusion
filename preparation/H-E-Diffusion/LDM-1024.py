import os
from dataclasses import dataclass

import torch
import torch.nn.functional as F
from PIL import Image
from accelerate import Accelerator
from datasets import load_dataset
from diffusers import DDPMPipeline, AutoencoderKL, LDMPipeline, DDPMScheduler, DDIMScheduler
from diffusers import UNet2DModel
from diffusers.optimization import get_cosine_schedule_with_warmup
import torch.types
from torchvision.transforms import v2
from tqdm.auto import tqdm
import wandb
from diffusers.utils.torch_utils import randn_tensor

wandEnabled = False

title = "05-14-VAE-Normalization"
# nohup python basic_training-LDM-1024.py > 05-14-VAE-Normalization/out.log 2>&1 &
os.makedirs(title, exist_ok=True)

@dataclass
class TrainingConfig:
    image_size = 1024  # the generated image resolution
    train_batch_size = 8
    eval_batch_size = 4  # how many images to sample during evaluation
    num_epochs = 100
    gradient_accumulation_steps = 1
    learning_rate = 1e-4
    lr_warmup_steps = 500
    save_image_epochs = 10
    save_model_epochs = 10
    mixed_precision = "fp16"  # `no` for float32, `fp16` for automatic mixed precision
    output_dir = title  # the model name locally and on the HF Hub
    overwrite_output_dir = True  # overwrite the old model when re-running the notebook
    seed = 0


config = TrainingConfig()

# start a new wandb run to track this script
if wandEnabled:
    wandb.init(
        # set the wandb project where this run will be logged
        project=title,

        # track hyperparameters and run metadata
        config={
        "learning_rate": config.learning_rate,
        "architecture": "LDM",
        "dataset": "1024 LDM H-E",
        "epochs": config.num_epochs,
        }
    )

dataset = load_dataset("./data", split="train")

vae = AutoencoderKL.from_pretrained("stabilityai/sd-vae-ft-mse")
vae.requires_grad_(False)
vae.to("cuda")

def decode_latents(latents):
    latents = 1 / vae.config.scaling_factor * latents
    image = vae.decode(latents, return_dict=False)[0]
    image = (image / 2 + 0.5).clamp(0, 1)
    # image = image.cpu().permute(0, 2, 3, 1).float()
    return image

transforms = v2.Compose([
    v2.RandomHorizontalFlip(),
    v2.RandomVerticalFlip(),    
    v2.ToImage(),
    v2.ToDtype(torch.float32, scale=True)
])

def transform(examples):
    images = [transforms(image.convert("RGB")) for image in examples["image"]]
    return {"images": images}


dataset.set_transform(transform)  # The transform is applied on-the-fly on batches when

train_dataloader = torch.utils.data.DataLoader(dataset, batch_size=config.train_batch_size, shuffle=True)

model = UNet2DModel(
    sample_size=128,  # the target image resolution
    in_channels=4,  # the number of input channels, 3 for RGB images
    out_channels=4,  # the number of output channels
    layers_per_block=2,  # how many ResNet layers to use per UNet block
    block_out_channels=(128, 128, 256, 256, 512, 512),  # the number of output channels for each UNet block
    down_block_types=(
        "DownBlock2D",  # a regular ResNet downsampling block
        "DownBlock2D",
        "DownBlock2D",
        "DownBlock2D",
        "AttnDownBlock2D",  # a ResNet downsampling block with spatial self-attention
        "DownBlock2D",
    ),
    up_block_types=(
        "UpBlock2D",  # a regular ResNet upsampling block
        "AttnUpBlock2D",  # a ResNet upsampling block with spatial self-attention
        "UpBlock2D",
        "UpBlock2D",
        "UpBlock2D",
        "UpBlock2D",
    ),
)
model.to("cuda")

# It is often a good idea to quickly check the sample image shape matches the model output shape:
# sample_image = dataset[0]["images"].unsqueeze(0)
# print("Input latent shape:", sample_image.shape)
# print("Output shape:", model(sample_image, timestep=0).sample.shape)

noise_scheduler = DDPMScheduler(num_train_timesteps=1000)

# First, you'll need an optimizer and a learning rate scheduler:
optimizer = torch.optim.AdamW(model.parameters(), lr=config.learning_rate)
lr_scheduler = get_cosine_schedule_with_warmup(
    optimizer=optimizer,
    num_warmup_steps=config.lr_warmup_steps,
    num_training_steps=(len(train_dataloader) * config.num_epochs),
)

def make_grid(images, rows, cols):
    w, h = images[0].size
    grid = Image.new("RGB", size=(cols * w, rows * h))
    for i, image in enumerate(images):
        grid.paste(image, box=(i % cols * w, i // cols * h))
    return grid

@torch.no_grad()
def evaluate(config, epoch, pipeline):
    latents = pipeline(
        batch_size=config.eval_batch_size,
        generator=torch.manual_seed(config.seed),
        return_dict=False,
        output_type="hans"
    )[0]
    latents = torch.from_numpy(latents).to("cuda")
    latents = torch.permute(latents, (0, 3, 1, 2))

    images = decode_latents(latents)
    images = v2.ToDtype(torch.uint8, scale=True)(images)
    images = list(map(lambda l: v2.ToPILImage()(l), list(images)))

    image_grid = make_grid(images, rows=2, cols=2)
    test_dir = os.path.join(config.output_dir, "samples")
    os.makedirs(test_dir, exist_ok=True)
    image_grid.save(f"{test_dir}/{epoch:04d}.png")


def train_loop(config, vae, model, noise_scheduler, optimizer, train_dataloader, lr_scheduler):
    # Initialize accelerator and tensorboard logging
    accelerator = Accelerator(
        mixed_precision=config.mixed_precision,
        gradient_accumulation_steps=config.gradient_accumulation_steps,
        log_with="tensorboard",
        project_dir=os.path.join(config.output_dir, "logs"),
    )

    if accelerator.is_main_process:
        if config.output_dir is not None:
            os.makedirs(config.output_dir, exist_ok=True)
        accelerator.init_trackers("train_example")

    # Prepare everything
    # There is no specific order to remember, you just need to unpack the
    # objects in the same order you gave them to the prepare method.
    vae, model, optimizer, train_dataloader, lr_scheduler = accelerator.prepare(
        vae, model, optimizer, train_dataloader, lr_scheduler
    )

    global_step = 0

    for epoch in range(config.num_epochs):
        progress_bar = tqdm(total=len(train_dataloader), disable=not accelerator.is_local_main_process)
        progress_bar.set_description(f"Epoch {epoch}")

        epochLosses = []

        for step, batch in enumerate(train_dataloader):
            # Convert images to latent space
            latents = vae.encode(batch["images"].to(dtype=torch.float32)).latent_dist.sample()
            latents = latents * vae.config.scaling_factor

            # Sample noise to add to the images
            # noise = torch.randn(latents.shape).to(latents.device)
            noise = torch.randn_like(latents).to(latents.device)
            bs = latents.shape[0]

            # Sample a random timestep for each image
            timesteps = torch.randint(
                0, noise_scheduler.config.num_train_timesteps, (bs,), device=latents.device
            ).long()

            # Add noise to the clean images according to the noise magnitude at each timestep
            # (this is the forward diffusion process)
            noisy_images = noise_scheduler.add_noise(latents, noise, timesteps)

            with accelerator.accumulate(model):
                # Predict the noise residual
                noise_pred = model(noisy_images, timesteps, return_dict=False)[0]
                loss = F.mse_loss(noise_pred, noise)
                epochLosses.append(loss)
                accelerator.backward(loss)

                accelerator.clip_grad_norm_(model.parameters(), 1.0)
                optimizer.step()
                lr_scheduler.step()
                optimizer.zero_grad()

            progress_bar.update(1)
            logs = {"loss": loss.detach().item(), "lr": lr_scheduler.get_last_lr()[0], "step": global_step}
            progress_bar.set_postfix(**logs)
            accelerator.log(logs, step=global_step)
            global_step += 1

        if wandEnabled:
            wandb.log({"epoch-mean-mse-loss": torch.tensor(epochLosses).mean()})

        # After each epoch you optionally sample some demo images with evaluate() and save the model
        if accelerator.is_main_process:
            pipeline = DDPMPipeline(unet=accelerator.unwrap_model(model), scheduler=noise_scheduler)

            if (epoch + 1) % config.save_image_epochs == 0 or epoch == config.num_epochs - 1:
                evaluate(config, epoch, pipeline)

            if (epoch + 1) % config.save_model_epochs == 0 or epoch == config.num_epochs - 1:
                pipeline.save_pretrained(config.output_dir)


pipeline = DDPMPipeline(unet=model, scheduler=noise_scheduler)
evaluate(config, 0, pipeline)
# pipeline.save_pretrained(config.output_dir)

# train_loop(config, vae, model, noise_scheduler, optimizer, train_dataloader, lr_scheduler)
