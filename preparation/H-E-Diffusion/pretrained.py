from diffusers import DDPMPipeline, AutoencoderKL, DDPMScheduler
import torch
from torchvision.transforms import v2
from diffusers.utils.torch_utils import randn_tensor
from PIL import Image

device = "cuda"

outputDir = "samplesPretrained/"
pipeline = DDPMPipeline.from_pretrained("./LDM-1024-fp16")
pipeline.to(device)

vae = AutoencoderKL.from_pretrained("stabilityai/sd-vae-ft-mse", torch_dtype=torch.float32)
vae.to(device)


def decode_img(latent):
    latent = latent.to(device).unsqueeze(0)
    # batch of latents -> list of images
    latent = (1 / 0.18215) * latent

    with torch.no_grad():
        image = vae.decode(latent).sample

    image = (image / 2 + 0.5).clamp(0, 1)
    image = image.detach()

    return image.squeeze()

def make_grid(images, rows, cols):
    w, h = images[0].size
    grid = Image.new("RGB", size=(cols * w, rows * h))
    for i, image in enumerate(images):
        grid.paste(image, box=(i % cols * w, i // cols * h))
    return grid

# Sample some images from random noise (this is the backward diffusion process).
# The default pipeline output type is `List[PIL.Image]`
@torch.no_grad()
def sample(imageName):
    image_shape = (1, 4, 128, 128)
    noise_scheduler = DDPMScheduler(num_train_timesteps=1000)
    latents = randn_tensor(image_shape, generator=None, device=torch.device("cuda"))

    for t in pipeline.progress_bar(pipeline.scheduler.timesteps):
        # 1. predict noise model_output
        model_output = pipeline.unet(latents, t).sample

        # 2. compute previous image: x_t -> x_t-1
        latents = noise_scheduler.step(model_output, t, latents, generator=None).prev_sample

    image = decode_img(latents.squeeze())

    transformToImage = v2.Compose([
        v2.ToDtype(torch.uint8, scale=True),
        v2.ToPILImage()
    ])

    image = transformToImage(image)

    image.save(outputDir + str(imageName) + ".png")


sample(1)