# Collection of relevant papers with short summaries
Filename format: ReleaseYear_CitationCount_Title

* model_Diffusion
    * **2015_3000_Deep_Unsupervised_Learning_using_Nonequilibrium_Thermodynamics**  
    The paper which set the foundation for diffusion models as we know today. Destroy structure in a data distribution through an iterative forward diffusion process. We then learn a reverse diffusion process that restores structure in data, yielding a highly flexible and tractable generative model of the data.

    * **2020_8000_DDPM.pdf**
    * **2021_100_UNIT-DDPM_UNpaired_Image_Translation_with_DDPM.pdf**
    * **2021_3000_Diffusion_Models_Beat_GANs_on_Image_Synthesis.pdf**
    * **2021_3100_score-based_generative_modeling_through_stochastic_differential_equations.pdf**
    * **2021_400_ILVR_Conditioning_Method_for_Denoising_Diffusion_Probabilistic_Models.pdf**  
    Introduces Iterative Latent Variable Refinement (ILVR), a method of conditioning the generative process of the unconditional DDPM model to generate images that share high-level semantics from given reference images without additional learning or models. Specifically, we refine each unconditional transition with a downsampled reference image.
    * **2022_100_Unsupervised_Medical_Image_Translation_With_Adversarial_DMs.pdf**
    * **2022_100_adapting_pretrained_vision-language_foundational_models_to_medical_imaging_domains.pdf**  
    Expand the capabilities of large pretrained foundation models (stable diffusion) to medical concepts, specifically for leveraging the Stable Diffusion model to generate domain-specific images found in medical imaging. The authors suggest that such models permit creation of high-fidelity synthetic datasets. Explores the sub-components of the Stable Diffusion pipeline (the variational autoencoder, the U-Net and the text-encoder) to fine-tune the model to generate medical images.
    * **2022_7000_High-Resolution_Image_Synthesis_with_LDM.pdf**
    * **2022_800_Elucidating_the_Design_Space_of_Diffusion-Based_Generative_Models.pdf**  
    Identifies the simple core mechanisms underlying the seemingly complicated approaches in the literature.
    * **2022_800_Palette_Image-to-Image_DM.pdf**
    * **2023_1000_Adding Conditional Control to Text-to-Image Diffusion Models.pdf**
    * **2023_100_dual_diffusion_implicit_bridges_for_image-to-image_translation.pdf**
    * **2023_50_Universal_Guidance_for_Diffusion_Models.pdf**
    * **2023_ALDM_for_3D_Medical_Image_to_Image_Translation_Multi-modal_MRI.pdf**  
    We introduce a switchable block that transfers the source latents to target-like latents using style transfer to enhance the performance of image-to-image translation. We leverage a LDM that translates a single source modality to various target modalities (one-to-many) in 3D medical images. Conditioning inspired by Palette and SPADE.
* model_GAN
    * **2014_GAN_Goodfellow.pdf**

    * **2014_GAN_conditionalGAN.pdf**
    * **2017_CycleGAN.pdf**
    * **2018_I2I-ConditionalGAN_pix2pix.pdf**
    * **2020_GAN_StargGAN_v2.pdf**
* model_VAE
    * **2018_VQVAE_Deepmind.pdf**

    * **2022_VQVAE.pdf**
* translation_image
    * **2017-unsupervised-image-to-image-translation-networks-Paper.pdf**

    * **2021-Multi_modal_image_matching.pdf**
* translation_staining
    * **2021_H-E-to-IHC_constrainedGAN.pdf**
    
    * **2023_H-E-to-IHC_Patch_Noise_Constrastive_Estimation.pdf**  
    Introduce a new loss function: Adaptive Supervised PatchNCE (ASP), to directly deal with the input to target inconsistencies in a proposed H&E-to-IHC image-to-image translation framework. Based on the realization that even when pairs of consecutive tissue slices do not yield images that are pixel-perfect aligned, it is highly likely that the corresponding patches in the two stains share the same diagnostic label. Multi-IHC Stain Translation (MIST) public dataset published. Breast Cancer Immunohistochemical BCI was used additionally.
    * **2023_H-E-to-IHC_StarGAN-v2.pdf**
    * **2023_H-E-to-IHC_comparison.pdf**  
    Comparison of 12 stain transfer approaches of which are 3 traditional image processing methods and 9 GAN-based image processing methods. The authors provide a framework to evaluate stain transfers methods. The used dataset is publicly available. The images are from mouse liver tissue samples stained with H&E and MT.
