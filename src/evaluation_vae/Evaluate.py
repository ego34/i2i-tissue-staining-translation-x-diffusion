import os
from glob import glob

import torch
from PIL import Image
from pytorch_msssim import ssim, ms_ssim
from torcheval.metrics import PeakSignalNoiseRatio
from torchvision.transforms import v2

from utility.fid_score import calculate_fid_given_paths

# config
outputDir = './output/sd-vae-original/*.jpg'


# import 10 sample images and transform to tensor (10, 3, 1024, 1024) from 0-255 float
transform = v2.Compose([v2.ToImage(), v2.ToDtype(torch.float32, scale=False)])

image_list_Raw = list(map(Image.open, glob('./data/train/*.jpg')))
image_list_transformed = transform(image_list_Raw)
X = torch.stack(transform(image_list_Raw), 0)

processed_list = list(map(Image.open, glob(outputDir)))
processed_list_transformed = transform(processed_list)
Y = torch.stack(processed_list_transformed, 0)



# Compute SSIM (Structural similarity index measure) values - paired
# Is a method for predicting the perceived quality
# The SSIM index is a full reference metric; in other words, 
# the measurement or prediction of image quality is based on an initial uncompressed or distortion-free image as reference.
ssim_val = ssim(X, Y, data_range=255, size_average=False)
ms_ssim_val = ms_ssim(X, Y, data_range=255, size_average=False)
print('ssim values: {}'.format(ssim_val))
print('ms_ssim values: {}'.format(ssim_val))

ssim = ssim(X, Y, data_range=255, size_average=True)
ms_ssim = ms_ssim(X, Y, data_range=255, size_average=True)
print('ssim average: {}'.format(ssim))
print('ms_ssim average: {}'.format(ms_ssim))
print("")



# Compute MSE
def mse_loss(img1, img2):
    return ((img1 - img2) ** 2).mean()

mseLossValues = []
for i, (x, y) in enumerate(zip(image_list_transformed, processed_list_transformed)):
    mseLossValues.append(mse_loss(x, y))

print("MSE Loss average: ", torch.stack(mseLossValues, 0).mean().item())
mseLossValues = list(map(torch.FloatTensor.item, mseLossValues))
print("MSE Loss values: ", mseLossValues)
print("")




# PSNR
metric = PeakSignalNoiseRatio()
metric.update(X, Y)
psnr = metric.compute()
print('PSNR: {:.2f}'.format(psnr))




# FID (Fréchet Inception Distance) - Unpaired
# FID is a measure of similarity between two datasets of images. 
# It was shown to correlate well with human judgement of visual quality 
# and is most often used to evaluate the quality of samples of Generative Adversarial Networks. 
# FID is calculated by computing the Fréchet distance between two Gaussians fitted to feature representations of the Inception network.
try:
    num_cpus = len(os.sched_getaffinity(0))
except AttributeError:
    num_cpus = os.cpu_count()

num_workers = min(num_cpus, 8) if num_cpus is not None else 0

fid2048 = calculate_fid_given_paths(["./data/train/", "./output/sdxl-vae-ft-mse/"], 1, "cuda", 2048, num_workers)
print("FID score 2048: ", fid2048)
