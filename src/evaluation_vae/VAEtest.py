from datasets import load_dataset
from diffusers import AutoencoderKL
from torch.utils.data import DataLoader
from torchvision.transforms import v2
import torch

device = "mps"
vae = AutoencoderKL.from_pretrained("stabilityai/sd-vae-ft-mse")
vae.to(device)

@torch.no_grad()
def encode(batch):
    latents = vae.encode(batch.to(dtype=torch.float32)).latent_dist.sample()
    latents = latents * vae.config.scaling_factor
    return latents


@torch.no_grad()
def decode_latents(batch):
    batch = batch / vae.config.scaling_factor
    images = vae.decode(batch, return_dict=False)[0]
    images = (images / 2 + 0.5).clamp(0, 1)
    return images


dataset = load_dataset("./data", split="train")
dataset.set_transform(v2.Compose([
    v2.ToTensor(),
        v2.RandomCrop((256, 256))]))
dataloader = DataLoader(dataset, batch_size=2, shuffle=False)
print("dataloader length: ", len(dataloader))


for index, batch in enumerate(dataloader):
    imageBatch = batch["image"].to(device)

    for index, image in list(imageBatch):
        v2.ToPILImage()(image).save(f"image{index}.png")

    # toImage = v2.Compose([
    #     v2.ToDtype(torch.uint8, scale=True),
    #     v2.ToPILImage()
    # ])
    #
    # for image in list(imageBatch):
    #     image = (image / 2 + 0.5).clamp(0, 1)
    #     image = toImage(image)
    #     image.save(f"{index}_raw.png")
    #
    # latents = encode(imageBatch)
    # images = decode_latents(latents)
    # for index, image in enumerate(list(images)):
    #     toImage(image).save(f"{index}_encdec.png")

    quit()


