from pathlib import Path

import torch
from datasets import load_dataset
from torch.utils.data import DataLoader
from torchvision.transforms import v2
from SDVAE import SDVAE

device = "mps"

dataset = load_dataset("./data/", split="train")
dataset.set_transform(v2.ToImage())

dataloader = DataLoader(dataset, batch_size=1, shuffle=False)
print("dataloader length: ", len(dataloader))

model = SDVAE(device)
outputDir = "./output/hmm/"
Path(outputDir).mkdir(parents=True, exist_ok=True)

for i, batch in enumerate(dataloader):
    image = batch["image"].to(device)
    rawInputFloat32 = v2.ToDtype(torch.float32, scale=True)(image)  # maps range from 0 - 255 to 0 - 1
    output = model.encodeDecode(rawInputFloat32)
    imageSavePath = outputDir + str(i) + ".jpg"
    output.save(imageSavePath)
    print("Image saved: ", imageSavePath)
