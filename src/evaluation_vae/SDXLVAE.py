import torch
from diffusers import  AutoencoderKL
from torchvision.transforms import v2


class SDXLVAE:
    # vae = AutoencoderKL.from_pretrained("madebyollin/sdxl-vae-fp16-fix", torch_dtype=torch.float16)
    vae = AutoencoderKL.from_pretrained("stabilityai/sd-vae-ft-mse", torch_dtype=torch.float32)

    def __init__(self, device):
        self.device = device
        self.vae.to(self.device)

    def encodeDecode(self, rawInputFloat32):
        latent = self.encode_img(rawInputFloat32)
        decoded = self.decode_img(latent).squeeze()

        decodedDenormalized = v2.ToDtype(torch.uint8, scale=True)(decoded)
        image = v2.ToPILImage()(decodedDenormalized)
        return image

    def encode_img(self, input_img):
        # Single image -> single latent in a batch (so size 1, 4, 64, 64)
        if len(input_img.shape) < 4:
            input_img = input_img.unsqueeze(0)

        with torch.no_grad():
            latent = self.vae.encode(input_img * 2 - 1)  # Note scaling

        return 0.18215 * latent.latent_dist.sample()

    def decode_img(self, latents):
        # bath of latents -> list of images
        latents = (1 / 0.18215) * latents

        with torch.no_grad():
            image = self.vae.decode(latents).sample

        image = (image / 2 + 0.5).clamp(0, 1)
        image = image.detach()

        return image
