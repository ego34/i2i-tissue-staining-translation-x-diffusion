from diffusers import AutoPipelineForText2Image
import torch

pipeline = AutoPipelineForText2Image.from_pretrained("runwayml/stable-diffusion-v1-5", torch_dtype=torch.float16).to("cuda")
pipeline.load_lora_weights("./output-1024/checkpoint-15000", weight_name="pytorch_lora_weights.safetensors")
pipeline.safety_checker = None
pipeline.requires_safety_checker = False

for i in range(10):
    image = pipeline("IHC stained tissue").images[0]
    image.save(f"lora_samples_1024_15000steps/{i}.png")
