from torchvision.transforms import v2
from dataclasses import dataclass
from datasets import load_dataset
import torch
from diffusers import UNet2DModel
from diffusers import DDPMScheduler
from diffusers.optimization import get_cosine_schedule_with_warmup
import torch.nn.functional as F
from accelerate import Accelerator
from tqdm.auto import tqdm
import os
from diffusers import DDPMPipeline
from diffusers import AutoencoderKL
from diffusers.utils.torch_utils import randn_tensor

import wandb

wandb_enabled = False
title = "LDM_05-06-2024"

@dataclass
class TrainingConfig:
    train_batch_size = 16
    eval_batch_size = 4
    num_epochs = 50
    gradient_accumulation_steps = 1
    learning_rate = 1e-4
    lr_warmup_steps = 500
    save_image_epochs = 10
    save_model_epochs = 30
    mixed_precision = "no"  # `no` for float32, `fp16` for automatic mixed precision
    output_dir = f"output_{title}"
    overwrite_output_dir = False
    seed = 0

config = TrainingConfig()

if wandb_enabled:
    wandb.init(
        project=title,
        config={
        "learning_rate": config.learning_rate,
        "architecture": "LDM",
        "dataset": "1024 LDM HE",
        "epochs": config.num_epochs,
        }
    )

preprocess = v2.Compose(
    [
        # transforms.Resize((config.image_size, config.image_size)),
        v2.RandomHorizontalFlip(),
        v2.RandomVerticalFlip(),
        v2.ToImage(),
        v2.ToDtype(torch.float32, scale=True)    ]
)

def transform(examples):
    images = [preprocess(image.convert("RGB")) for image in examples["image"]]
    return {"images": images}


dataset = load_dataset("../../data", split="train")
dataset.set_transform(transform)
train_dataloader = torch.utils.data.DataLoader(dataset, batch_size=config.train_batch_size, shuffle=True)

vae = AutoencoderKL.from_pretrained("stabilityai/sd-vae-ft-mse")
vae.to("cuda")

model = UNet2DModel(
    sample_size=128,  # the target image resolution
    in_channels=4,  # the number of input channels, 3 for RGB images
    out_channels=4,  # the number of output channels
    layers_per_block=2,  # how many ResNet layers to use per UNet block
    block_out_channels=(128, 128, 256, 256, 512, 512),  # the number of output channels for each UNet block
    down_block_types=(
        "DownBlock2D",  # a regular ResNet downsampling block
        "DownBlock2D",
        "DownBlock2D",
        "DownBlock2D",
        "AttnDownBlock2D",  # a ResNet downsampling block with spatial self-attention
        "DownBlock2D",
    ),
    up_block_types=(
        "UpBlock2D",  # a regular ResNet upsampling block
        "AttnUpBlock2D",  # a ResNet upsampling block with spatial self-attention
        "UpBlock2D",
        "UpBlock2D",
        "UpBlock2D",
        "UpBlock2D",
    ),
)
model.to("cuda")

noise_scheduler = DDPMScheduler(num_train_timesteps=1000)
optimizer = torch.optim.AdamW(model.parameters(), lr=config.learning_rate)
lr_scheduler = get_cosine_schedule_with_warmup(
    optimizer=optimizer,
    num_warmup_steps=config.lr_warmup_steps,
    num_training_steps=(len(train_dataloader) * config.num_epochs),
)


def make_grid(images, rows, cols):
    w, h = images[0].size
    grid = Image.new("RGB", size=(cols * w, rows * h))
    for i, image in enumerate(images):
        grid.paste(image, box=(i % cols * w, i // cols * h))
    return grid


@torch.no_grad()
def decode_latents(latents):
    latents = 1 / vae.config.scaling_factor * latents
    image = vae.decode(latents, return_dict=False)[0]
    image = (image / 2 + 0.5).clamp(0, 1)
    return image.squeeze()


@torch.no_grad()
def evaluate(config, epoch, pipeline):
    output_dir = os.path.join(config.output_dir, f"samples/epoch{epoch:04d}")
    os.makedirs(output_dir, exist_ok=True)

    for i in range(2):
        noise_scheduler = DDPMScheduler(num_train_timesteps=1000)
        latents = randn_tensor((1, 4, 128, 128), generator=None, device=torch.device("cuda"))

        for t in pipeline.progress_bar(pipeline.scheduler.timesteps):
            model_output = pipeline.unet(latents, t).sample # 1. predict noise model_output
            latents = noise_scheduler.step(model_output, t, latents, generator=None).prev_sample # 2. compute previous image: x_t -> x_t-1

        image = decode_latents(latents)
        image = v2.ToDtype(torch.uint8, scale=True)(image)
        image = v2.ToPILImage()(image)
        image.save(f"{output_dir}/{i:02d}.png")


    # latents = pipeline(
    #     batch_size=config.eval_batch_size,
    #     generator=torch.manual_seed(config.seed),
    # ).latents

    # latents = torch.from_numpy(latents).to("cuda")
    # latents = torch.permute(latents, (0, 3, 1, 2))

    # images = decode_latents(latents)
    # images = v2.ToDtype(torch.uint8, scale=True)(images)
    # images = list(map(lambda l: v2.ToPILImage()(l), list(images)))

def train_loop(config, model, noise_scheduler, optimizer, train_dataloader, lr_scheduler):
    accelerator = Accelerator(
        mixed_precision=config.mixed_precision,
        gradient_accumulation_steps=config.gradient_accumulation_steps,
        log_with="tensorboard",
        project_dir=os.path.join(config.output_dir, "logs"),
    )
    if accelerator.is_main_process:
        os.makedirs(config.output_dir, exist_ok=True)
        accelerator.init_trackers("train_example")

    vae, model, optimizer, train_dataloader, lr_scheduler = accelerator.prepare(
        vae, model, optimizer, train_dataloader, lr_scheduler
    )

    global_step = 0

    for epoch in range(config.num_epochs):
        progress_bar = tqdm(total=len(train_dataloader), disable=not accelerator.is_local_main_process)
        progress_bar.set_description(f"Epoch {epoch}")

        for step, batch in enumerate(train_dataloader):
            with torch.no_grad():
                latents = vae.encode(batch["images"].to(dtype=torch.float32)).latent_dist.sample()
            latents = latents * vae.config.scaling_factor

            noise = torch.randn_like(latents).to(latents.device)
            bs = latents.shape[0]

            # Sample a random timestep for each image
            timesteps = torch.randint(
                0, noise_scheduler.config.num_train_timesteps, (bs,), device=latents.device
            ).long()

            # Add noise to the clean images according to the noise magnitude at each timestep
            # (this is the forward diffusion process)
            noisy_images = noise_scheduler.add_noise(latents, noise, timesteps)

            with accelerator.accumulate(model):
                # Predict the noise residual
                noise_pred = model(noisy_images, timesteps, return_dict=False)[0]
                loss = F.mse_loss(noise_pred, noise)
                accelerator.backward(loss)

                accelerator.clip_grad_norm_(model.parameters(), 1.0)
                optimizer.step()
                lr_scheduler.step()
                optimizer.zero_grad()

            progress_bar.update(1)
            logs = {"loss": loss.detach().item(), "lr": lr_scheduler.get_last_lr()[0], "step": global_step}
            progress_bar.set_postfix(**logs)
            accelerator.log(logs, step=global_step)
            global_step += 1

        if wandb_enabled:
            wandb.log({"epoch-mean-mse-loss": torch.tensor(epochLosses).mean()})

        # After each epoch you optionally sample some demo images with evaluate() and save the model
        if accelerator.is_main_process:
            pipeline = DDPMPipeline(unet=accelerator.unwrap_model(model), scheduler=noise_scheduler)

            if (epoch + 1) % config.save_image_epochs == 0 or epoch == config.num_epochs - 1:
                evaluate(config, epoch, pipeline)

            if (epoch + 1) % config.save_model_epochs == 0 or epoch == config.num_epochs - 1:
                pipeline.save_pretrained(config.output_dir)


def test_inference_and_safe():
    pipeline = DDPMPipeline(unet=model, scheduler=noise_scheduler)
    evaluate(config, 0, pipeline)
    pipeline.save_pretrained(config.output_dir)


# test_inference_and_safe()
train_loop(config, model, noise_scheduler, optimizer, train_dataloader, lr_scheduler)
