import torch

# Compute MSE
def mse_loss(img1, img2):
    return ((img1 - img2) ** 2).mean()

def evaluate_mse_loss(sourceList, targetList):
    mseLossValues = []
    for i, (x, y) in enumerate(zip(sourceList, targetList)):
        mseLossValues.append(mse_loss(x, y))

    averageLoss = torch.stack(mseLossValues, 0).mean().item()
    mseLossValues = list(map(torch.FloatTensor.item, mseLossValues))

    return averageLoss, mseLossValues
