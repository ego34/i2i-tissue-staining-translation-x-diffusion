from pytorch_msssim import ssim, ms_ssim


# Compute SSIM (Structural similarity index measure) values - paired
# Is a method for predicting the perceived quality
# The SSIM index is a full reference metric; in other words,
# the measurement or prediction of image quality is based on an initial uncompressed or distortion-free image as reference.
def evaluateSSIM(X, Y, outputFilePath):
    ssim_val = ssim(X, Y, data_range=255, size_average=False)
    ms_ssim_val = ms_ssim(X, Y, data_range=255, size_average=False)
    print('ssim values: {}'.format(ssim_val))
    print('ms_ssim values: {}'.format(ssim_val))

    ssim = ssim(X, Y, data_range=255, size_average=True)
    ms_ssim = ms_ssim(X, Y, data_range=255, size_average=True)
    print('ssim average: {}'.format(ssim))
    print('ms_ssim average: {}'.format(ms_ssim))
