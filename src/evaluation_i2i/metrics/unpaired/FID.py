import os

from evaluation.utility.fid_score import calculate_fid_given_paths

# from ...utility.fid_score import calculate_fid_given_paths
# FID (Fréchet Inception Distance) - Unpaired
# FID is a measure of similarity between two datasets of images.
# It was shown to correlate well with human judgement of visual quality
# and is most often used to evaluate the quality of samples of Generative Adversarial Networks.
# FID is calculated by computing the Fréchet distance between two Gaussians fitted to feature representations of the Inception network.

def calculateFID(sourceDirPath, targetDirPath):
    try:
        num_cpus = len(os.sched_getaffinity(0))
    except AttributeError:
        num_cpus = os.cpu_count()

    num_workers = min(num_cpus, 8) if num_cpus is not None else 0

    fid2048 = calculate_fid_given_paths([sourceDirPath, targetDirPath], 1, "cuda", 2048, num_workers)
    return fid2048