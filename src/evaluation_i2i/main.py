import sys
from pathlib import Path
import torch
from PIL import Image
from torchvision.transforms import v2
import json

from metrics.unpaired.FID import calculateFID
from report import ReportUnconditional

sourceFolderPath = sys.argv[1] if Path(sys.argv[1]).is_absolute() else Path.cwd() / sys.argv[1]
targetFolderPath = sys.argv[2] if Path(sys.argv[2]).is_absolute() else Path.cwd() / sys.argv[2]
reportFileName = sys.argv[3]

if not Path(sourceFolderPath).is_dir():
    sys.exit("Source folder does not exist!")

if not Path(targetFolderPath).is_dir():
    sys.exit("Target folder does not exist!")

def prepareData(sourceFolderPath, targetFolderPath):
    transform = v2.Compose([v2.ToImage(), v2.ToDtype(torch.float32, scale=False)])

    sourceList = list(map(lambda imagePath: Image.open(imagePath.absolute()), Path(sourceFolderPath).glob('**/*')))
    sourceListTransformed = transform(sourceList)
    X = torch.stack(sourceListTransformed, 0)

    targetList = list(map(lambda imagePath: Image.open(imagePath.absolute()), Path(targetFolderPath).glob('**/*')))
    targetListTransformed = transform(targetList)
    Y = torch.stack(targetListTransformed, 0)

    return X, Y, sourceListTransformed, targetListTransformed


def writeReportToJSON(report, reportFileName):
    jsonString = json.dumps(report.__dict__, indent=2)
    reportsFolder = Path.cwd() / "reports"
    reportsFolder.mkdir(parents=True, exist_ok=True)
    with open(f"{reportsFolder}/{reportFileName}.json", "w") as reportFile:
        reportFile.write(jsonString)
        reportFile.close()


def evaluateUnconditional(sourceFolderPath, targetFolderPath, reportFileName):
    X, Y, sourceListTransformed, targetListTransformed = prepareData(sourceFolderPath, targetFolderPath)

    fid = calculateFID(sourceFolderPath, targetFolderPath)
    report = ReportUnconditional(
        fid,
        0
    )
    writeReportToJSON(report, reportFileName)


evaluateUnconditional(sourceFolderPath, targetFolderPath, reportFileName)