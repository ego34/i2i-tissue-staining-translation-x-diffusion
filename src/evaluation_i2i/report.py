from dataclasses import dataclass


@dataclass(unsafe_hash=True)
class ReportUnconditional:
    fid: float
    kid: float


@dataclass(unsafe_hash=True)
class ReportPaired:
    mseAverage: float
    mseErrors: [float]
    PSNR: float
    SSIM: float
    WD: float
