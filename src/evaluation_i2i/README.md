# Evaluation framework for both unconditional image generation and paired image generation

### Usage
- requirements
  - `src/` and `target/` folder have to contain the same number of images of same resolution
  - for paired evaluation the pair of source image and generated image need to have the same name
  - the glob pattern only searches for *.jpg images

`python evaluation.py ./src ./target "report-name"`